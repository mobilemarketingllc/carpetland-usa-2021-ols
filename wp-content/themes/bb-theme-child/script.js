jQuery(document).on('click', '.choose_location', function() {   
 
    jQuery.cookie("preferred_store", null, { path: '/' });       
    jQuery.cookie("preferred_city", null, { path: '/' }); 
    jQuery.cookie("preferred_distance", null, { path: '/' });   
  
    var home = jQuery(this).attr("data-home"); 
    var distance = jQuery(this).attr("data-distance");  
  
    
    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        dataType: 'JSON',
        data: 'action=choose_location&store_id=' + jQuery(this).attr('id'),
  
        success: function(data) {
            
               var locdata = JSON.parse(JSON.stringify(data));
               var location_phone = locdata.header_phone;     
               var locname = locdata.header_short_name;          
               var loc_address = locdata.header_address+'<br>'+locdata.header_city+', '+locdata.header_state+' '+locdata.header_zipcode;
  
               
  
               jQuery(".location_name").html(locdata.header_short_name);
               jQuery(".retailerName").html(locdata.header_short_name);               
               jQuery("span.retailer_name").html(locdata.header_short_name);   
               jQuery("span.street_address").html(locdata.header_address); 
               jQuery("span.city").html(locdata.header_city);       
               jQuery("span.state").html(locdata.header_state); 
               jQuery("span.zipcode").html(locdata.header_zipcode);       
                     
               jQuery(".loc_address").html(loc_address);
               
               var for_ph = '<a href="tel:'+location_phone+'"><i class="fa fa-phone-alt"></i> &nbsp;'+location_phone+'</a>';
               jQuery(".loc_phone").html(for_ph); 
  
  
               jQuery.cookie("preferred_store", locdata.store_id, { expires: 1, path: '/' });
               jQuery.cookie("preferred_city", locdata.header_city, { expires: 1, path: '/' });
               jQuery.cookie("preferred_distance", distance, { expires: 1, path: '/' });
  
               jQuery(".uabb-offcanvas-close").trigger("click");              
  
              //  if(locname.search("Carpetland") == 0){
                
              //   jQuery("#site_logo .uabb-photo-img").attr("src","https://americasflooringstore-stg.mm-dev.agency/wp-content/uploads/2021/09/carpetland_usa_logo.png");
  
              //  }else if(locname.search("America’s") == '-1'){                
                
              //   jQuery("#site_logo .uabb-photo-img").attr("src","https://americasflooringstore-stg.mm-dev.agency/wp-content/uploads/2021/08/americas-flooring-store-reverse.png");
              //  }
  
  
           
        }
    });
  
  });
  
  jQuery(document).ready(function() {
  
    var mystore_loc = jQuery(".header_location_name").html();
    jQuery("#input_29_32").val(mystore_loc);
   
    var preferred_city = jQuery.cookie("preferred_city");
    var preferred_store = jQuery.cookie("preferred_store");
    var preferred_distance = jQuery.cookie("preferred_distance");
  
   
      
    
    jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=get_storelisting',
       dataType: 'JSON',
       success: function(response) {
          
               var posts = JSON.parse(JSON.stringify(response));
               var header_data = posts.header;
               var list_data = posts.list;
               var header_phone = posts.header_phone;
               var locname = posts.store_location;          
               
               var loc_address = posts.address+'<br>'+posts.city+', '+posts.state+' '+posts.postal_code;
  
              // jQuery("#closest_location_distance span.uabb-heading-text").html(posts.distance +' miles away'); 
              // jQuery("#closest_location h3.uabb-infobox-title-prefix").html(posts.store_name);         
              // jQuery("#closest_location h4.uabb-infobox-title").html(loc_address);          
             //  jQuery("#closest_location a.uabb-button").attr( "href", posts.store_url);                 
               
  
               var mystore_loc =  jQuery.cookie("preferred_storename");      
               
               if(preferred_store == null){
  
                jQuery.cookie("preferred_store", posts.store_id, { expires: 1, path: '/' });
                jQuery.cookie("preferred_city", posts.city, { expires: 1, path: '/' });
                jQuery.cookie("preferred_distance", posts.distance, { expires: 1, path: '/' });
                
                
               }            
             
              
               jQuery(".location_div").html(header_data);
                   
               jQuery("#ajaxstorelisting").html(list_data);   
               
              //  if(locname.search("Carpetland") == 0){                  
                
              //   jQuery("#site_logo .uabb-photo-img").attr("src","https://americasflooringstore-stg.mm-dev.agency/wp-content/uploads/2021/09/carpetland_usa_logo.png");
  
              //  }else if(locname.search("America’s") == '-1'){                
                
              //   jQuery("#site_logo .uabb-photo-img").attr("src","https://americasflooringstore-stg.mm-dev.agency/wp-content/uploads/2021/08/americas-flooring-store-reverse.png");
               
              //  }
             
       }
   });
  
  });
  
  
  
  
  function addFavProduct(is_fav, sku, prod_id) {
  
  var queryString = "";
  queryString = 'action=add_fav_product&sku=' + sku + '&post_id=' + prod_id + '&is_fav=' + is_fav;
  jQuery.ajax({
      url: "/wp-admin/admin-ajax.php",
      data: queryString,
      type: "POST",
      dataType: "json",
      success: function(response) {
  
          if (is_fav == 0) {
              is_fav1 = 1;
          } else {
              is_fav1 = 0;
          }
  
          $(".is_fav" + sku).attr("onClick", "addFavProduct('" + is_fav1 + "','" + sku + "','" + prod_id + "')");
          $(".is_fav" + sku).removeClass("is_fav0");
          $(".is_fav" + sku).removeClass("is_fav1");
          $(".is_fav" + sku).toggleClass("is_fav" + is_fav1);
      },
      error: function() {}
  });
  }
  
  // function printDiv(divName) {
  
  //     var printContents = document.getElementById("printMe").innerHTML;
  //     var originalContents = document.body.innerHTML;
  
  //     document.body.innerHTML = printContents;
  
  //     window.print();
  
  //     document.body.innerHTML = originalContents;
  
  // }
  
  
  function getDataUrl(img) {
  // Create canvas
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');
  // Set width and height
  canvas.width = img.width;
  canvas.height = img.height;
  // Draw the image
  ctx.drawImage(img, 0, 0);
  return canvas.toDataURL('image/jpeg');
  }
  
  function getDataUrlTwo(img) {
  // Create canvas
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');
  // Set width and height
  canvas.width = 200;
  canvas.height = 200;
  // Draw the image
  ctx.drawImage(img, 0, 0);
  return canvas.toDataURL('image/jpeg');
  }
  
  function provideUrls() {
  if (document.querySelector('.printDOc')) {
      document.querySelector('.printDOc').addEventListener('click', function(event) {
          event.preventDefault()
          var img = document.querySelectorAll('.printWrap img');
  
          img.forEach(async function(ele) {
              ele.setAttribute("crossOrigin", "Anonymous");
              ele.src = await getDataUrlTwo(ele);
          });
  
          document.querySelector('.hideShare').style.display = 'none';
          printDiv();
          document.querySelector('.hideShare').style.display = 'flex';
      });
  }
  }
  
  function provideUrlsTwo() {
  document.querySelector('.printDOc').addEventListener('click', function() {
      var img = document.querySelector('.printWrap img');
      img.src = getDataUrl(img);
      printDiv();
  });
  }
  window.onload = setTimeout(provideUrls, 1000);
  
  
  function printDiv() {
  var printContents = document.querySelector(".printWrap");
  var opt = {
      margin: 0.3,
      filename: 'download.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { scale: 2 },
      jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
  };
  
  // New Promise-based usage:
  html2pdf().from(printContents).set(opt).save();
  }
  
  jQuery(document).on('click', '.deletemeasure', function() {
  
  var img_id = jQuery(this).attr("data-id");
  
  jQuery.ajax({
      type: "POST",
      url: "/wp-admin/admin-ajax.php",
      data: 'action=delete_measureimg&mimg_id=' + img_id,
  
      success: function(data) {
  
          jQuery("#mesureMentprintMe").html(data);
  
      }
  });
  
  });
  
  function displayPopUpInq() {
  jQuery('.productInquery').each(function() {
      jQuery(this).on('click', function() {
          jQuery('.overlayInquery').css("display", 'block');
          var pageUrl = jQuery(this).attr('data-page');
          var collection = jQuery(this).attr('data-collection');
          var brand = jQuery(this).attr('data-brand');
          var img = jQuery(this).attr('data-img');
  
          jQuery('.pageUrl input[type=text]').val(pageUrl);
          jQuery('.collectionName input[type=text]').val(collection);
          jQuery('.brandName input[type=text]').val(brand);
  
          jQuery('.productInfo img').attr('src', img);
          jQuery('.productInfo h3').text(collection);
          jQuery('.productInfo p').text(brand);
      })
  })
  }
  displayPopUpInq();
  
  function openPopUp(e) {
  
  let title = e.currentTarget;
  console.log(e);
  let element = document.querySelector('.overlayMeasure');
  element ? element.remove() : console.log('not findout');
  
  var product = `<div class="overlayMeasure"><div class='measurepopup printWrap'>
  <div class='row'>
  <div class='col-lg-6'>
  <h3>${e.getAttribute("data-title")}</h3>
  </div>
  <div class='col-lg-6 sharingBox hideShare'>
  <a href='#' id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin">
  </a>
              
          <a href='#' class="printDOc" >
              <i class='fa fa-print' aria-hidden='true'></i>
          </a>
          <a href='javascript:void(0);' onclick="closePopup()">
              <i class='fa fa-times' aria-hidden='true'></i>
          </a>	
      </div>
  </div>
  
  <div id="measureprint">
   <img src='${e.getAttribute("data-img")}' </div></div> </div>`;
  
  jQuery('body').append(product);
  provideUrlsTwo();
  //new needShareDropdown(document.getElementById('share-button-3'));
  
  new needShareDropdown(document.getElementById('share-button-3'));
  removeInnerHtml();
  }
  
  function removeInnerHtml() {
  var button = document.querySelector('.need-share-button_button');
  if (button) {
      button.innerHTML = '';
  }
  }
  removeInnerHtml();
  
  
  window.onload = function checkShareBox() {
  var button = document.querySelector('#share-button-3');
  if (button) {
      new needShareDropdown(document.getElementById('share-button-3'));
      removeInnerHtml();
  }
  }
  
  function closePopup() {
  document.querySelector('.overlayMeasure').remove();
  }
  
  function closepopupoverlay() {
  document.querySelector('.overlayInquery').style.display = 'none';
  }
  
  
  var debounce = (info, delay) => {
  let debounceTimer
  return function() {
      clearTimeout(debounceTimer)
      debounceTimer = setTimeout(() => addRemovefavAjax(info), delay)
  }
  }
  
  function addRemovefavAjax(classcontainer) {
  console.log('run run');
  if (classcontainer.status == 'add') {
      jQuery.ajax({
          type: "POST",
          url: "/wp-admin/admin-ajax.php",
          data: 'action=add_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,
  
          success: function(data) {
              jQuery(classcontainer.element).removeClass("add_Fav");
              jQuery(classcontainer.element).addClass("rem_fav");
              jQuery(classcontainer.element).children("i").removeClass("fa-heart-o");
              jQuery(classcontainer.element).children("i").addClass("fa-heart");
  
          }
      });
  } else {
      jQuery.ajax({
          type: "POST",
          url: "/wp-admin/admin-ajax.php",
          data: 'action=remove_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,
  
          success: function(data) {
              jQuery(classcontainer.element).removeClass("rem_fav");
              jQuery(classcontainer.element).addClass("add_Fav");
              jQuery(classcontainer.element).children("i").removeClass("fa-heart");
              jQuery(classcontainer.element).children("i").addClass("fa-heart-o");
  
          }
      });
  }
  
  }
  
  jQuery(document).on('click', '.favProdPdp', function(event) {
  event.stopPropagation();
  console.log(event)
  var current = this;
  var post_id = jQuery(this).attr("data-id");
  
  var classContainer = {
      postId: jQuery(this).attr("data-id"),
      UserId: jQuery(this).attr("data-user"),
      element: this
  }
  
  if (jQuery(current).hasClass("add_Fav")) {
      classContainer.status = "add";
      var add = debounce(classContainer, 300);
      add();
  } else {
      classContainer.status = "remove";
      var remove = debounce(classContainer, 300);
      remove();
  }
  });
  
  
  jQuery(document).on('click', '#rem_fav', function() {
  jQuery.ajax({
      type: "POST",
      url: "/wp-admin/admin-ajax.php",
      data: 'action=remove_favroiute_list&post_id=' + jQuery(this).attr("data-id") + '&user_id=' + jQuery(this).attr("data-user_id"),
  
      success: function(data) {
          jQuery("#ajaxreplace").html(data);
          location.reload();
      }
  });
  });
  
  
  
  jQuery(".add_note_fav").each(function() {
  jQuery(this).on("click", function() {
      var proid = jQuery(this).attr("data-productid");
      jQuery('#addnote_productid').val(proid);
  });
  })
  jQuery(".view_note_fav").each(function() {
  jQuery(this).on("click", function() {
      var note_val = jQuery(this).attr("data-note");
      var getP = jQuery('.view_note_wrapper-overlay .uabb-modal-content-data p:first-child');
      getP.text(note_val);
      getP.addClass('note_content');
  });
  
  });
  
  
  /*:::::::::::::::fav rpoduct and Measuremnt js :::::::::::::::*/
  jQuery('#add_note_form').submit(function(e) {
  e.preventDefault();
  
  var message = jQuery('textarea#message').val();
  var proid = jQuery('#addnote_productid').val();
  //alert('message');
  message = jQuery.trim(message);
  if (message) {
      jQuery.ajax({
          data: { action: 'add_note_form', note: message, addnote_productid: proid },
          type: 'post',
          url: "/wp-admin/admin-ajax.php",
          success: function(data) {
              console.log(data);
              location.reload();
          }
      });
  }
  })
  
  function ValidateTextarea(e) {
  console.log('Hiii');
  }
  
  
  jQuery(document).ready(function() {
  
  jQuery.ajax({
      type: "POST",
      url: "/wp-admin/admin-ajax.php",
      data: 'action=distance_function',
      dataType: 'JSON',
      success: function(response) {
         
              var posts = JSON.parse(JSON.stringify(response));
              console.log(posts);
  
                   jQuery(".rockford_distance h5.uabb-heading ").html(posts.rockford_distance +' miles away'); 
                   jQuery(".sycamore_distance h5.uabb-heading ").html(posts.sycamore_distance +' miles away'); 
  
                   
              
            
      }
  });
  });